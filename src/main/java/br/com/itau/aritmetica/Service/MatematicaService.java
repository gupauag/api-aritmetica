package br.com.itau.aritmetica.Service;

import br.com.itau.aritmetica.DTOs.EntradaDTO;
import br.com.itau.aritmetica.DTOs.RespostaDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO){
        int num = 0;
        for(int n: entradaDTO.getNumeros()){
            num += n;
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(num);
        return respostaDTO;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO){
        int num = 0;
        for(int n: entradaDTO.getNumeros()){
            if (n == entradaDTO.getNumeros().get(0)) num = n;
            else num -= n;

        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(num);
        return respostaDTO;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO){
        int num = 1;
        for(int n: entradaDTO.getNumeros()){
            num = num*n;
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(num);
        return respostaDTO;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO){
        int num = 0;
        for(int n: entradaDTO.getNumeros()){
            if (n == entradaDTO.getNumeros().get(0)) num = n;
            else if(n < num){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "numeros precisam ser crescentes, pós divisão");
            }else{
                num = n/num;
            }
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(num);
        return respostaDTO;
    }

}
