package br.com.itau.aritmetica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AritmeticaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AritmeticaApplication.class, args);
	}

}
