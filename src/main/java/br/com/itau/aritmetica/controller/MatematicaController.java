package br.com.itau.aritmetica.controller;

import br.com.itau.aritmetica.DTOs.EntradaDTO;
import br.com.itau.aritmetica.DTOs.RespostaDTO;
import br.com.itau.aritmetica.Service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    //    private MatematicaService matematicaService = new MatematicaService();
    // o spring instancia automaticamente o q for declarado como @Service para @Autowide
    @Autowired
    private MatematicaService matematicaService;


    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO) {
        validaEntrada(entradaDTO);
        RespostaDTO resposta = matematicaService.soma(entradaDTO);

        return resposta;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO) {
        validaEntrada(entradaDTO);
        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);

        return resposta;
    }

    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO) {
        validaEntrada(entradaDTO);
        RespostaDTO resposta = matematicaService.multiplicacao(entradaDTO);

        return resposta;
    }

    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO) {
        validaEntrada(entradaDTO);
        RespostaDTO resposta = matematicaService.divisao(entradaDTO);

        return resposta;
    }

    public void validaEntrada(EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros a serem somados");
        }
        for (int n : entradaDTO.getNumeros()) {
            if (n <= 0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não é permitido enviar numeros negativos ou zero");
        }
    }

//    @GetMapping("/olaMundo")
//    public String olaMundo(){
//        return "Ola mundo. Bem vindo";
//    }
//
//
//    @DeleteMapping("ola2")
//    public String ola2(){
//        return "Retorno 2";
//    }

}
