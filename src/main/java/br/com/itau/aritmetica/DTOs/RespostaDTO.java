package br.com.itau.aritmetica.DTOs;

public class RespostaDTO {
    private int resultado;

    public RespostaDTO() {

    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }
}
