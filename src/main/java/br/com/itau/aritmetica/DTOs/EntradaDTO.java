package br.com.itau.aritmetica.DTOs;

import java.util.List;

public class EntradaDTO {
    private List<Integer> numeros;

    public EntradaDTO() {

    }

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}
